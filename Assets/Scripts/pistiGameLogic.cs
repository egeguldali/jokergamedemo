﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class pistiGameLogic : MonoBehaviour {



    public player p1;    //Player
    public player p2;   // Computer

    public GameObject[] cards;      // Array of playing cards
    int n = 51;             //index of cards[]
    private GameObject randomCard;    //Random card picked from cards array
    


    public GameObject[] playerHand;     // Holds Array of card positions in player's hand.
    public GameObject[] computerHand;   //Holds Array of card positions in computer's hand.

    public GameObject table;        //Objects rather than panel. 

    int middlePoint = 0;            // Total Point can be taken if a player hits.
    int middleCardAmount = 0;       // Card Amount can be taken if a player hits.


    int lastPicker; // 0 if player takes, 1 if computer takes;

    public GameObject middleCard;       //Holds card position in the middle
    public GameObject[] beginningRightCard;     //Holds 3 closed cards in first turn

    public Button[] playerHandButtons;      //Buttons positioned to card in players hand.


    public Text numberCardLeftText;     //Text of how many cards left in the deck
    public Text p1PointText;            // Text of players points
    public Text p2PointText;            // Text of computers point

    public Text winnerMessage;          // Text of who is the winner


    public GameObject endingPanel;      // Game Ending Panel
    public Text scoreBoard;             // Score Board

    bool gameJustFinish = true;         //If the game just finished. Used for manuplating update function.

     
     int turn = 0;

	void Start () {
	}
  

    void Update () {

        int m = n + 1;                  // n+1 card left in deck
        string cardLeftString = m.ToString();
        numberCardLeftText.text ="Deste(" + cardLeftString + ")";

        p1PointText.text = "Oyuncu Puanı(" + p1.point + ")";
        p2PointText.text = "Bilgisayar Puanı(" + p2.point + ")";
        
        if(isHandsEmpty() == true && m > 0)         // if hands empty but there are cards left in deck
        {
            drawCards();
        }
        else if(isHandsEmpty() == false && m == 0)
        {
        }
        else if(isHandsEmpty() == true && m == 0)       // if hands empty and there are no card left in deck
        {
            gameEnd();
        }

    }

    /* showACard() is algoritm of picking a random card from deck without picking same cards. 
     * Array index "n" is the range of cards that has not seen yet. When a card picked it change place with cards[n] and n dcreases 1.*/
    public void showACard()     
    {
        GameObject clone;
        GameObject dummy;
        GameObject temp;
        Random rnd = new Random();
        int randomCardIndex = Random.Range(0, n);

        dummy = cards[n];
        temp = cards[randomCardIndex];

        clone = Instantiate(cards[randomCardIndex], randomCard.transform.position, randomCard.transform.rotation);

        randomCard = clone;

        cards[randomCardIndex] = dummy;
        cards[n] = temp;

        temp = null;
        dummy = null;
        n = n - 1;
    }
    /* drawCards() function is giving each player 4 random and unseen cards. For first turn it also gives 3 closed and 1 opened card in middle.*/
    public void drawCards()
    {
        p1.playerHandCardAmount = 4;
        p1.playerHandCardAmount = 4;
        for(int i = 0; i <= 3; i++)         //Give 4 cards to player.
        {

            randomCard = playerHand[i];
            showACard();
            playerHand[i] = randomCard;
            playerHand[i].SetActive(true);
            playerHandButtons[i].interactable = true;
            
                      
        }
        for (int i = 0; i <= 3; i++)        //Give 4 cards to computer.
        {
            randomCard = computerHand[i];
            showACard();
            computerHand[i] = randomCard;
            computerHand[i].SetActive(true);
        }

        if (turn == 0)              // If it is first turn give 3 cards to middle closed.
        {
            for (int i = 0; i <= 2; i++)
            {
                randomCard = beginningRightCard[i];
                showACard();
                beginningRightCard[i] = randomCard;
                beginningRightCard[i].SetActive(true);
                middlePoint = middlePoint + getCardPoint(beginningRightCard[i]);
                middleCardAmount++;
            }
        }

        if (turn == 0)      // If it is first turn give a card to middle opened.
        {
            randomCard = middleCard;
            showACard();
            middleCard = randomCard;
            middleCard.SetActive(true);
            middlePoint = middlePoint + getCardPoint(middleCard);
            middleCardAmount++;
            turn++;
        }

       
    }



    public int getCardPoint(GameObject cardToCheck)         //Returns the point of a card.
    {     
        if (cardToCheck.GetComponent<card>().value == 2 && cardToCheck.GetComponent<card>().className == "C")
        {
            return 2;

        }
        else if(cardToCheck.GetComponent<card>().value == 1 || cardToCheck.GetComponent<card>().value == 11)
        {

            return 1;

        }
        if (cardToCheck.GetComponent<card>().value == 10 && cardToCheck.GetComponent<card>().className == "D")
        {
            return 3;
        }

        else
        {

            return 0;
        }
    }


    public void playACard(GameObject cardToPlay, player p)          // A Player plays A Card
    {
        p.playerHandCardAmount--;           // When a player plays a card its card amount in hand decreases
            
        if (!hitCheck(cardToPlay))          // If played card is not a hit, then it placed in the middle
        {
            GameObject clone;
            clone = Instantiate(cardToPlay, middleCard.transform.position, middleCard.transform.rotation);
            middleCard.SetActive(false);
            middleCard = clone;
            middlePoint += getCardPoint(middleCard);
            middleCardAmount++;
            cardToPlay.SetActive(false);
        }
                
       else if (hitCheck(cardToPlay))           // If played card is a hit middle point and card amount added to players. Card place to Win Card place. It also changes the lastPicker number.
        {
            for(int i = 0; i <= 2; i++)
            {
                beginningRightCard[i].SetActive(false);
            }

            middleCard.SetActive(false);
            middleCard.GetComponent<card>().value = 0;
            cardToPlay.SetActive(false);

            GameObject clone;
            clone = Instantiate(cardToPlay, p.WinCard.transform.position,p.WinCard.transform.rotation);
            clone.transform.localScale = new Vector3(15,15,15);

            p.WinCard.SetActive(false);
            p.WinCard = clone;

            p.WinCard.SetActive(true);

            p.point += middlePoint;
            p.cardAmount += middleCardAmount + 1;

            if(middleCardAmount == 1)
            {
                Debug.Log("Pisti");
                if(cardToPlay.GetComponent<card>().value == 11)
                {
                    p.point += 20;
                }
                else
                {
                    p.point += 10;
                }
            }

            middlePoint = 0;
            middleCardAmount = 0;

            if (p == p1)
            {
               
                lastPicker = 0;
            }
            else if(p == p2)
            {
                
                lastPicker = 1;
            }
            
        }

        


    }


    int k = 3;

    public void ComputerPlayACard()         //Simple computer play. Computer always plays according to his handArray.
    {
        
        
        computerHand[k].transform.rotation = Quaternion.Euler(180, 0, 0);
        playACard(computerHand[k], p2);
        computerHand[k].transform.rotation = Quaternion.Euler(0, 0, 0);
        k--;
        if(k < 0)
        {
            k = 3;
        }
      
    }

    public void playFirstCard()     // Player selects first card then AI plays
    {
        playACard(playerHand[0],p1);
        playerHandButtons[0].interactable = false;
        ComputerPlayACard();
        
    }

    public void playSecondCard()    // Player selects second card then AI plays 
    {
        playACard(playerHand[1],p1);
        playerHandButtons[1].interactable = false;
        ComputerPlayACard();

    }

    public void playThirdCard()     // Player selects third card then AI plays
    {
        playACard(playerHand[2],p1);
        playerHandButtons[2].interactable = false;
        ComputerPlayACard();
    }

    public void playFourthCard()        // Player selects fourth card then AI plays
    {
        playACard(playerHand[3],p1);
        playerHandButtons[3].interactable = false;
        ComputerPlayACard();
    }

    public bool hitCheck(GameObject CardToPlay)     //Checks if played card hits to middle card.
    {

        if (CardToPlay.GetComponent<card>().value == middleCard.GetComponent<card>().value || CardToPlay.GetComponent<card>().value== 11)
        {
            if (middleCardAmount != 0)
                return true;
            else
                return false;
        }

        else
            return false;
   }


    public bool isHandsEmpty()      // Checks if both players hand is empty.
    {
        if (p1.playerHandCardAmount <= 0 && p2.playerHandCardAmount <= 0)
            return true;

        else
            return false;
    }


 //Game Ending, gives all point and cards to last picker, determines who is the winner and closes all objects to shows End Game Panel.
    public void gameEnd()      
    {
        if(lastPicker == 0)
        {
            p1.cardAmount += middleCardAmount;
           
            middleCard.SetActive(false);
            lastPicker = -1;
        }
        else if(lastPicker == 1)
        {
            p2.cardAmount += middleCardAmount;
          
            middleCard.SetActive(false);
            lastPicker = -1;
        }

        if (gameJustFinish == true)
        {
            if (p1.cardAmount > p2.cardAmount)
            {
                p1.point += 3;
            }
            else if (p2.cardAmount > p1.cardAmount)
            {
                p2.point += 3;
            }
            gameJustFinish = false;
        }


        table.SetActive(false);

        p1PointText.gameObject.SetActive(false);
        p2PointText.gameObject.SetActive(false);
        numberCardLeftText.gameObject.SetActive(false);
        p1.WinCard.SetActive(false);
        p2.WinCard.SetActive(false);

        endingPanel.SetActive(true);

        if (p1.point > p2.point)
        {
            winnerMessage.text = "Player Won!";
        }
        else if(p2.point > p1.point)
        {
            winnerMessage.text = "Computer Won!";

        }
        else if (p2.point > p1.point)
        {
            winnerMessage.text = "DRAW!";

        }
        scoreBoard.text = "Player Point: " + p1.point + "\n" + "Computer Point: " + p2.point;
    }

    public void restartGame()       //Restart the game
    {
        Application.LoadLevel(Application.loadedLevel);
    }
   

}
